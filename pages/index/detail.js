var app = getApp()
var fun = require('../fun.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nav_top:app.globalData.nav_top,nav_title:'商品详情',nav_icon:'icon-chevron-left',nav_ac:'back',hb_index:0
  },

  onLoad: function (op) {
var id=op.id;
var from_id=op.from_id;if(!from_id){from_id='';}
var hd_id=op.hd_id;if(!hd_id){hd_id='';}
var from=op.from;if(!from){from='direct';}
if (op.scene) {//扫描
  var scene = decodeURIComponent(op.scene);
  console.log("scene is ", scene);
  var arrPara = scene.split("&");
  console.log(arrPara)
  var arr = [];
  for (var i in arrPara) {
    arr = arrPara[i].split("=");
    if (arr[0] == 'f') { from_id = arr[1]; }//from_id
    if (arr[0] == 'id') { id=arr[1]; }//from_id

  }
 from='scan';
}
if(from_id){app.globalData.from_id=from_id;}
if(from){app.globalData.from=from;}//从哪里来
this.setData({id:id,hd_id:hd_id,from_id:app.globalData.from_id});
console.log(app.globalData.from_id);
var that=this;
wx.getLocation({//获取位置
  type: 'wgs84',
  success (res) {
    that.setData({latitude:res.latitude,longitude:res.longitude})

  }
 })
this.setData({direct_page:encodeURIComponent('/pages/index/detail?id='+id)});
var direct=op.direct;
if(direct){console.log('derect',decodeURIComponent(decodeURIComponent(direct)))
this.setData({direct:decodeURIComponent(decodeURIComponent(direct))})
}
  },

  onReady: function () {
    console.log('page',getCurrentPages().length);
    var page_num=getCurrentPages().length;
    if(page_num==1){
    this.setData({nav_icon:'icon'})
    }
  },


  onShow: function () {
this.load();
this.setData({mid_win:''});
var time=(new Date).getTime();
var user=app.globalData.user_info;if(!user){user=[]}
app.globalData.view=[{page:'detail',pro_id:this.data.id,from:app.globalData.form,from_id:app.globalData.from_id?app.globalData.from_id:0,
time:time,user_id:app.globalData.user_id?app.globalData.user_id:0,user_session:app.globalData.user_session,web:app.globalData.web,ac:'load',
nickname:user.nickname?user.nickname:'',logo:user.logo?user.logo:''
}]
console.log('load',app.globalData.view);
  },


  load: function (e) {
var id=this.data.id;
var that=this;
var hd_id=this.data.hd_id;if(!hd_id){hd_id=''}
var from_id=app.globalData.from_id;
if(!from_id){from_id='';}

wx.request({
  url: app.globalData.server,
  data:{
    ac:'get_detail',user_id:app.globalData.user_id,session:app.globalData.session,id:id,hd_id:hd_id,
    web:app.globalData.web,from_id:from_id
  },success(res){
if(!res.data){return;}console.log(res.data);
if(res.data.err=='no_login'){that.setData({login_show:'show'})}//login

that.setData({
pro:res.data.pro,
xuzhi:res.data.xuzhi,
quan:res.data.quan,
login_bg:res.data.login_bg,
xihuan_title:res.data.xihuan_title,
pros:res.data.pros,
cart_num:res.data.cart_num,
fuwu:res.data.fuwu,
send_info:res.data.send_info,
shop:res.data.shop,
hd:res.data.hd,
hd_list:res.data.hd_list,
user:res.data.user,
pj:res.data.pj
})

if(res.data.hd){
  clearInterval(that.data.setInter)
  that.data.setInter=setInterval(function(){that.set_time()},1000);
}

var shop=res.data.shop;
if(shop){
  app.globalData.view[0]['shop_id']=shop.id;
if(shop.longitude && shop.latitude){
var latitude=that.data.latitude;
var longitude=that.data.longitude;
if(longitude && latitude){
var dis=fun.get_dis(latitude,longitude,shop.latitude,shop.longitude);
that.setData({dis:dis})
}

}

}
that.action('加载完成')
var user=res.data.user;
if(user){
  app.globalData.view[0]['nickname']=user.nickname;
app.globalData.view[0]['logo']=user.logo;
}else{
  app.globalData.view[0]['nickname']='游客';
}

  }
})

  },set_time(){//倒计时
    var hd=this.data.hd;
    if(!hd){return;}
var time=Math.ceil((new Date()).getTime()/1000);

var t=parseInt(hd.end);
var start=parseInt(hd.start);
var x=t-time;
hd['time_text']='活动距离截止';
if(x<=0){hd['time_text']='活动已结束';}else{
if(start>time){hd['time_text']='距离开始';
x=start-time;
}

var day=Math.floor(x/(3600*24));
x=x-day*3600*24;
var hour=Math.floor(x/3600);
var x=x-hour*3600;
if(hour>0 && hour<10){hour='0'+hour;}
var minit=Math.floor(x/60);
var x=x-minit*60;
if(minit>0 && minit<10){minit='0'+minit;}
if(x<10){x='0'+x;}
hd['day']=day;hd['hour']=hour;hd['minit']=minit;hd['second']=x;
}

  this.setData({hd:hd})
  },

  quan_box: function () {//优惠券
    this.action('打开优惠券')
var that=this;
var pro=this.data.pro;
var user_id=app.globalData.user_id;
//if(!user_id){this.setData({login_show:'show'});return;}
wx.request({
  url: app.globalData.server,
  data:{
    ac:'get_quan',user_id:app.globalData.user_id,session:app.globalData.session,id:pro.user_id
  },success(res){
  if(!res.data){return;}console.log(res.data);
  if(res.data.err!='ok'){
  if(res.data.err=='no_login'){that.setData({login_show:'show'});return;}
  app.err(res.data.err);return;
  }
that.setData({
quan_list:res.data.list,
quan_show:'show'
})

  }
})

  },
  
  login: function (e) {
    var c = e.currentTarget.dataset.c;
    if(c=='close'){this.setData({login_show:'hide'});return;}
    fun.go_login().then(res => {
      if (res == 'ok') {
        app.msg('登录成功');
        this.load();
        this.setData({ login_show: 'hide' });
      }
    });

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  onShareAppMessage: function () {this.action('分享按钮')
    var pro=this.data.pro;
    var user_id=app.globalData.user_id;if(!user_id){user_id='';}
    var url='/pages/index/detail?id='+pro.id+'&from_id='+user_id+'&from=share';
    var num=Math.random();
    var img_num= pro.img.length;
    if(img_num>1){
    img_num=Math.floor(img_num*num);
    }else{
      img_num=0;
    }
console.log(url,num,img_num);
this.setData({share_box_show:''})
    return {
      title: pro.title,
      path: url,
      imageUrl: pro.img[img_num],

    }
  },onShareTimeline: function () {this.action('分享到朋友圈')
    var pro=this.data.pro;
    var user_id=app.globalData.user_id;if(!user_id){user_id='';}
    var url='/pages/index/detail?id='+pro.id+'&from_id='+user_id+'&from=share';
    var num=Math.random();
    var img_num= pro.img.length;
    if(img_num>1){
    img_num=Math.floor(img_num*num);
    }else{
      img_num=0;
    }
console.log(url,num,img_num);
this.setData({share_box_show:''})
    return {
      title: pro.title,
      path: url,
      imageUrl: pro.img[img_num],

    }
  },go(e){this.action('打开页面:'+e.currentTarget.dataset.url)
wx.navigateTo({
  url: e.currentTarget.dataset.url,
})

  },
  
  back(e){this.action('返回')
    wx.navigateBack({
      delta: 0,
    })
  },home(e){this.action('回首页')
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },rgo(e){this.action('跳转页面:'+e.currentTarget.dataset.url)
  console.log(e.currentTarget.dataset.url);
    wx.redirectTo({
      url: e.currentTarget.dataset.url,
    })
  },
  
  get_quan(e){
var quan_list=this.data.quan_list;
var index=e.currentTarget.dataset.index;
if(!quan_list){app.err('没有找到优惠券');return;}
console.log(quan_list[index]);

var that=this;this.action('获取优惠券');
if(quan_list[index]['my_get']==1){return;}
wx.request({
  url: app.globalData.server,
  data:{
    ac:'quan_get',user_id:app.globalData.user_id,session:app.globalData.session,id:quan_list[index]['id']
  },success(res){
if(!res.data){return;}console.log(res.data);
if(res.data.err!='ok'){
  if(res.data.err=='no_login'){that.setData({login_show:'show'});return;}
  app.err(res.data.err);return;
}
quan_list[index]['my_get']=1
that.setData({quan_list:quan_list});
app.msg('领券成功');
  }
})




  },map_go(){this.action('打开地图');
var pro=this.data.pro;
let plugin = requirePlugin('routePlan');
let key =app.globalData.map_key; 
let referer = '优选';   
let endPoint = JSON.stringify({  
  'name': pro.addr, 
  'latitude':pro.latitude,
  'longitude': pro.longitude
});
wx.navigateTo({
  url: 'plugin://routePlan/index?key=' + key + '&referer=' + referer + '&endPoint=' + endPoint
});


  },select_size(e){//选择尺寸
    this.action('选择尺寸');
var select_pro=this.data.select_pro;
var index=e.currentTarget.dataset.index;
var list_select=select_pro.list_select;
for (var i in list_select){
if(i==index){
if(list_select[i].num<1){return;}else{
  list_select[index].is_select='is_select';
}
}else{
  list_select[i].is_select='';
}

}
select_pro.select_num=1;
select_pro.list_select=list_select;
this.setData({select_pro:select_pro});
this.format_cart()
  },select_color(e){this.action('选择颜色');
  var index=e.currentTarget.dataset.index;
  var select_pro=this.data.select_pro;
 var list=select_pro.list;
for(var i in list){
if(i==index){
  list[i].is_select='is_select';
  var list_select=list[i].size;
  console.log(list_select);
  var xx=0;
  for(var j in list_select){
  if(list_select[j].num>0 && xx==0){list_select[j].is_select='is_select';xx++;}else{
    list_select[j].is_select='';
  }
  
  }

}else{
list[i].is_select='';
}
}
select_pro.select_num=1;
select_pro.list_select=list_select;
select_pro.list=list;
this.setData({select_pro:select_pro});
this.format_cart()

  },num_add(e){this.action('增加数量');
var select_pro=this.data.select_pro;
var d=parseInt(e.currentTarget.dataset.d);
var pro_max=parseInt(this.data.pro_max);
console.log(d)
var num=parseInt(select_pro.select_num);
num+=d;
if(num<1){num=1;}

if(num>pro_max){app.msg('超过库存');return;}

select_pro.select_num=num;
this.setData({select_pro:select_pro})

  },format_cart(){//对订单进行分析
var select_pro=this.data.select_pro;
var list=select_pro.list;
var list_select=select_pro.list_select;
var cart_selectd='';
var price=select_pro.price;
for(var i in list){
  if(list[i].is_select=='is_select'){
    cart_selectd+=i;
  }
}
var pro_max=0;
for(var i in list_select){
  if(list_select[i].is_select=='is_select'){
    cart_selectd+=' '+list_select[i].size;
    price=list_select[i].price;
    pro_max=list_select[i].num;
  }
}

var pp=fun.number_fromat(price,2);
var p=pp.split('.');

select_pro.p1=p[0];select_pro.p2=p[1];
select_pro.cart_selectd=cart_selectd;
this.setData({select_pro:select_pro,pro_max:pro_max})
  
  },add_cart(e){this.action('添加购物车');
    var user_id=app.globalData.user_id;
    if(!user_id){this.setData({login_show:'show'});return;}
    var shop=this.data.shop;
  var select_pro=this.data.select_pro;
  var cart_method=e.currentTarget.dataset.c;
  if(!select_pro){select_pro=this.data.pro;}
var s_id=0;
var num=parseInt(select_pro.select_num);if(!num){num=1;}
var max=parseInt(select_pro.total);
//if(num>max){app.msg('超过库存');return;}
//list_select
var list_select=select_pro.list_select;console.log(list_select);
if(list_select){
for(var i in list_select){
if(list_select[i].is_select=='is_select'){
  console.log(list_select[i]);
s_id=list_select[i].id;
}
}
}
console.log(s_id);
var that=this;
wx.request({
  url: app.globalData.server,
  data:{
    ac:'cart_add_fav',user_id:app.globalData.user_id,session:app.globalData.session,pro_id:select_pro.id,s_id:s_id,num:num
  },success(res){
if(!res.data){return;}console.log(res.data);
if(res.data.err!='ok'){
if(res.data.err=='no_login'){that.setData({login_show:'show'});return;}
app.err(res.data.err);return;
}

that.setData({cart_num:res.data.cart_num,cart_box_show:''});
//app.msg('添加成功');

if(cart_method=='buy'){//直接下单
wx.navigateTo({
  url: '/pages/index/cart?cc=pro&shop_id='+shop.id+'&pro_id='+select_pro.id,
})
}else{
that.setData({mid_win:'show'})
}
  }
})

  },cart_box_close(){this.setData({cart_box_show:''})},
  cart_show(e){this.action('打开购物车');
    var c=e.currentTarget.dataset.c;if(!c){c=''}//是否直接购买
  var pro=this.data.pro;
  this.setData({cart_box_show:'show',select_pro:pro,cart_method:c});
  this.format_cart();
  },close(e){
  var c=e.currentTarget.dataset.c;
  this.setData({[c]:''})
  },win_open(e){
    var c=e.currentTarget.dataset.c;
    this.action('打开窗口'+c);
    console.log(c);
  this.setData({[c]:'show'})
  },share(){this.action('打开海报');
var pro=this.data.pro;
var that=this;
console.log('share');
wx.showToast({
  title: '海报创建中',
  icon: 'success',
  duration: 2000
});
this.setData({share_box_show:''})
wx.request({
  url: app.globalData.server,
  data:{
ac:'get_hb',user_id:app.globalData.user_id,session:app.globalData.session,
id:pro.id

  },success(res){
if(!res.data){return;}console.log(res.data);
if(res.data.err!='ok'){
  if(res.data.err=='no_login'){that.setData({login_show:'show'});return;}
  app.err(res.data.err);return;}
that.setData({share_imgs:res.data.share_imgs,share_box_show:''})

  }
})


  },save_img(e){this.action('保存海报');

    var that=this;
    var index=e.currentTarget.dataset.index;
    console.log(index);
    var share_imgs=this.data.share_imgs;
    var img=share_imgs[index];
    if(!img){return;}
    
    wx.showLoading({
      title: '保存中...', 
      mask: true,
    });
    wx.downloadFile({
      url:img,
      success: function(res) {
        if (res.statusCode === 200) {
          let img = res.tempFilePath;
          wx.saveImageToPhotosAlbum({
            filePath: img,
            success(res) {
              

              wx.showToast({
                title: '保存成功',
                icon: 'success',
                duration: 2000
              });
            },
            fail(res) {
             
              wx.showToast({
                title: '保存失败',
                icon: 'success',
                duration:1000
              });
            }
          });
        }
      }
    });
  },onHide(){

    console.log(app.globalData.view);
    this.action('close');
    var data=JSON.stringify(app.globalData.view)
    app.globalData.view=[];
    wx.request({
      url: app.globalData.server+'?ac=set_view',
      data:{
        data:data
      },
      method:'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success(res){
        console.log(res.data)
      }
    })
                            },action(ac){//添加操作
                              var time=(new Date).getTime()
                              app.globalData.view.push({ac:ac,time:time});
                            },get_power(e){
                              var hd=this.data.pro;
                                      var user_id=hd.user_id;
                                     
                                      var that=this;
                                      wx.request({
                                        url: app.globalData.server,
                                        data:{
                                      ac:'get_power',user_id:app.globalData.user_id,session:app.globalData.session,id:user_id,
                                      
                                      
                                        },success(res){
                                      
                                          if(!res.data){return;}console.log(res.data);
                                          if(res.data.err!='ok'){
                                          if(res.data.err=='no_login'){that.setData({login_show:'show'});return}
                                          app.err(res.data.err);return;
                                          }
                                          
                                      app.msg('权限获取成功');
                                      app.globalData.user_id = res.data.user.id;
                                                        app.globalData.session = res.data.user.session;
                                                        app.globalData.user_info = res.data.user;
                                                        //缓存
                                                        //var save_data = { user_id: res.data.id, session: res.data.session, logo: res.data.logo, nickname: res.data.nickname };
                                                        res.data.user.user_id = res.data.user.id;
                                      
                                                        console.log('保存登录状态' + JSON.stringify(res.data.user))
                                                        try {
                                                          wx.setStorageSync('login_info', JSON.stringify(res.data.user))
                                                        } catch (e) { }
                                                        that.load();
                                        
                                        }
                                      })
                                      
                                      
                                                      },
                                                      img_view(e){
var img=e.currentTarget.dataset.v;
wx.previewImage({
  urls: [img],
})

                                                      },pj_show(e){
                                                      var v=e.currentTarget.dataset.v;
                                                      this.setData({pj_show:v})
                                        
                                                      },change_hb(e){
console.log(e);
var index=e.detail.current;
      this.setData({hb_index:index})                                                
                                                      }
})