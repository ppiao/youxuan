var app = getApp()
var fun = require('../fun.js');
Page({

  data: {
    nav_top:app.globalData.nav_top,nav_title:'品牌库',nav_icon:'icon-chevron-left',nav_ac:'back',page:1,can_load:1
  },
  onLoad: function (op) {
    var cc=op.c;if(!cc){cc='';}
    var s=op.s;if(!s){s='';}
    
    app.globalData.can_load=1;
    var shop_id=op.shop_id;if(!shop_id){shop_id='';}
    var from_id=op.from_id;
    if(!from_id){from_id=0;}else{
      app.globalData.from_id=from_id;
    }
    var brand=op.brand;if(!brand){brand=''}
    this.setData({cc:cc,s:s,shop_id:shop_id,select_brand:brand});

  },

  onLoad: function (options) {
app.globalData.can_load=1;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.load();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  load: function () {
var that=this;
var page=this.data.page;if(!page){page=1;}
var c=this.data.c;if(!c){c=''}
if(app.globalData.can_load!=1){return;}
app.globalData.can_load=0;
var key=this.data.key;if(!key){key='';}
wx.request({
  url: app.globalData.server,
data:{
ac:'get_brand_list',page:page,c:c,key:key
},success(res){
if(!res.data){return;}console.log(res.data);
if(res.data.err!='ok'){
if(res.data.err=='no_login'){that.setData({login_show:'show'});return}
app.err(res.data.err);return;
}
if(page==1){
  that.setData({list:res.data.list?res.data.list:'',bottom_index:res.data.bottom_index,c_list:res.data.c_list});
}else{
  var l=res.data.list;
  if(!l){return;}
  var list=that.data.list;
  for(var i in l){
    list.push(l[i])
  }
that.setData({list:list})
}

app.globalData.can_load=1;
}
})
  },
  load_more(e){
if(app.globalData.can_load==1){
var page=this.data.page;
if(!page){page=1;}
page++;
this.setData({page:page})
this.load();
}


  },
  search: function (e) {
    var key=e.detail.value;
    app.globalData.can_load=1;
    this.setData({page:1,key:key,c:''})
this.load()
  },

 
  select_c: function (e) {
    var key=e.currentTarget.dataset.c;
    app.globalData.can_load=1;
    this.setData({page:1,c:key})
this.load()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }, go(e){
    wx.navigateTo({
      url: e.currentTarget.dataset.url,
    })
    
      },
      
      back(e){
        wx.navigateBack({
          delta: 0,
        })
      },home(e){
        wx.reLaunch({
          url: '/pages/index/index',
        })
      },rgo(e){
        wx.redirectTo({
          url: e.currentTarget.dataset.url,
        })
      },login: function (e) {
        var c = e.currentTarget.dataset.c;
        if(c=='close'){this.setData({login_show:'hide'});return;}
        fun.go_login().then(res => {
          if (res == 'ok') {
            app.msg('登录成功');
            this.load();
            this.setData({ login_show: 'hide' });
          }
        });
    
      },close(e){
        var c=e.currentTarget.dataset.c;
        this.setData({[c]:''})
        },win_open(e){
          var c=e.currentTarget.dataset.c;
          console.log(c);
        this.setData({[c]:'show'})
        }
})